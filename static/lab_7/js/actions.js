var $ = jQuery.noConflict();
$(document).ready(function() {
    $(".accordionhead > a").on("click", function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).siblings(".content").slideUp();
        }
        else{
            $(".accordionhead > a").removeClass("active");
            $(this).addClass("active");
            $(".content").slideUp();
            $(this).siblings(".content").slideDown();
        }
    });

    $("#btn-mode").click(function(){
        if($(this).hasClass("active")){
            $(this).removeClass("active");
            $(this).text("Default mode");
            $(".bg_prtf").css("background-color", "deepskyblue");
            $(".accordionhead").css("background-color", "#92dff3");
            $(".accordionhead > a").css({"border-bottom": "1px solid #398ad7", "color" : "white"})
            $(".title_white").css("color", "white");
            $(".accordionhead > a.active").css("background-color", "#ffffff");
        }
        else{
            $(this).removeClass("active");
            $(this).addClass("active");
            $(this).text("Dark mode");
            $(".bg_prtf").css("background-color", "black");
            $(".accordionhead").css("background-color", "black");
            $(".accordionhead > a").css({"border-bottom" : "1px solid white", "color" : "red"})
            $(".title_white").css("color", "red");
            $(".accordionhead > a.active").css("background-color", "#ffffff");
        }
    });
});