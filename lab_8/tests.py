from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from .views import *

# Create your tests here.
class BooksUnitTest(TestCase):
    def test_booklist_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_booklist_using_booklist_func(self):
        found = resolve('/books/')
        self.assertEqual(found.func, book_list)

    def test_booklist_using_booklist_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_title_in_template(self):
        response = Client().get('/books/')
        html_response = response.content.decode()
        self.assertIn("It's (not) a library!", html_response)

    def test_table_created(self):
        response = Client().get('/books/')
        html_response = response.content.decode()
        self.assertIn('Title', html_response)