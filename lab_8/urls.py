from django.urls import *

from . import views

app_name = 'lab_8'
urlpatterns = [
    path('', views.book_list, name='book_list'),
    path('<title>', views.books, name='booksdict')
]