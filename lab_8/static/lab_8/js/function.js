function getbooks(title){
    $("#book_table").empty();
    $.ajax({
        url: title,
        method: "GET",
        type: "json",
        success: function(result){
            var table_content = "";
            var items = result.items
            console.log(items)
            for (i=0; i < items.length; i++){
                var data = items[i]
                table_content += '<tr>';
                table_content += '<th scope="row">' + (i+1) + '</th>';
                if(data.volumeInfo.imageLinks != null){
                    table_content += '<td><img src=' + data.volumeInfo.imageLinks.thumbnail + '></td>';
                }
                else{
                    table_content += '<td>Preview is currently not available</td>';
                }
                table_content += '<td>' + data.volumeInfo.title + '</td>';
                if (data.volumeInfo.authors != null){
                    table_content += '<td>' + data.volumeInfo.authors + '</td>';
                }
                else{
                    table_content += '<td>Unknown</td>';
                }
                table_content += '</tr>';
            }
            $('#book_table').append(table_content);
        }
    });
}

function getInput(){
    var title = $('#input').val();
    $('#input').val("");
    getbooks(title)
}