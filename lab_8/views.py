from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
import json
import requests

# Create your views here.
# def books(request):
#     r = requests.get("https://www.googleapis.com/books/v1/volumes?q='manusia'")
#     r = r.json()
#     items = r['items'] #google api store item details as list of dictionary
#     shown_items = []
#     for i in items:
#         temp = {
#             'preview' : i['volumeInfo']['imageLinks']['thumbnail'],
#             'title' : i['volumeInfo']['title']
#         }
#         getAuthor = i['volumeInfo'].get('authors', 'Unknown')
#         temp['authors'] = getAuthor
#         shown_items.append(temp)    #remove unnecessary details that is not shown
#     return JsonResponse({'items' : shown_items})

def books(request, title):
    r = requests.get("https://www.googleapis.com/books/v1/volumes?q=intitle:" + title)
    r = r.json()
    return JsonResponse(r)

def book_list(request):
    return render(request, 'books.html')