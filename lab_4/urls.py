from django.urls import *

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('schedules', views.schedules, name='schedules'),
    path('schedules/list', views.schedulelist, name='schedulelist'),
    re_path('deleteschedule/(?P<idnum>\d+)/', views.deleteschedule, name='deleteschedule'),
]