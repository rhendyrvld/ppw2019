from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from datetime import date

# Create your tests here.

class LabTest(TestCase):

    #main
    def test_lab_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_lab_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_lab_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
    
    #schedule page
    def test_schedules_url_exist(self):
        response = Client().get('/schedules')
        self.assertEqual(response.status_code, 200)

    def test_schedules_using_schedules_func(self):
        found = resolve('/schedules')
        self.assertEqual(found.func, schedules)

    def test_schedules_using_schedules_template(self):
        response = Client().get('/schedules')
        self.assertTemplateUsed(response, 'schedules.html')

    def test_valid_form_input(self):
        form_data = {
            'hari' : 'senin',
            'tanggal' : '11/8/2019',
            'jam' : '14:30',
            'nama_kegiatan' : 'menabung',
            'tempat' : 'Pacil',
            'kategori' : 'penting'
        }

        schd = ScheduleForm(data = form_data)
        self.assertTrue(schd.is_valid())

    #schedulelist page
    def test_schedulelist_url_exist(self):
        response = Client().get('/schedules/list')
        self.assertEqual(response.status_code, 200)

    def test_schedulelist_using_schedulelist_func(self):
        found = resolve('/schedules/list')
        self.assertEqual(found.func, schedulelist)

    def test_schedulelist_using_schedulelist_template(self):
        response = Client().get('/schedules/list')
        self.assertTemplateUsed(response, 'schedulelist.html')

    #deleteschedule
    def test_schedule_deleted(self):
        response = Client().get('/schedule/list')
        schd = Schedule.objects.create(id=2, hari='Senin', tanggal='2019-11-08', jam='9:45', nama_kegiatan='AAA', tempat='Rumah', kategori='penting')
        Schedule.objects.get(id=2).delete()
        self.assertNotIn('Senin', response.content.decode())

    #models
    def test_model_can_create_new_schedule(self):
        schd = Schedule.objects.create(hari='Senin', tanggal='2019-11-08', jam='9:45', nama_kegiatan='AAA', tempat='Rumah', kategori='penting')
        count_table = Schedule.objects.all().count()
        self.assertEqual(count_table, 1)

    def test_model_returns_nama_kegiatan(self):
        schd = Schedule.objects.create(hari='Senin', tanggal='2019-11-08', jam='9:45', nama_kegiatan='AAA', tempat='Rumah', kategori='penting')
        self.assertEqual(str(schd), 'AAA')
    
    #form
    def test_schedule_in_form_post(self):
        form_data = {
            'hari' : 'senin',
            'tanggal' : '11/8/2019',
            'jam' : '14:30',
            'nama_kegiatan' : 'menabung',
            'tempat' : 'Pacil',
            'kategori' : 'penting'
        }
        response = Client().post('/schedules', form_data)
        list_response = Client().get('/schedules/list')
        self.assertIn('menabung', list_response.content.decode())
    
    def test_schedule_not_in_form_post(self):
        form_data = {
            'hari' : 'senin',
            'tanggal' : '11/8/2019',
            'jam' : '14:30,2',
            'nama_kegiatan' : 'menabung',
            'tempat' : 'Pacil',
            'kategori' : 'penting'
        }
        response = Client().post('/schedules', form_data)
        list_response = Client().get('/schedules/list')
        self.assertNotIn('menabung', list_response.content.decode())
    