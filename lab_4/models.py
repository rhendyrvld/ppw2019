from django.db import models
from django.utils import timezone
from datetime import *

# Create your models here.

class Schedule(models.Model):
    hari = models.CharField(max_length=30)
    tanggal = models.DateField()
    jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=30)
    kategori = models.CharField(max_length=30)

    def __str__(self):
        return self.nama_kegiatan