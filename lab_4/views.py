from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Schedule
from .forms import ScheduleForm


def index(request):
    return render(request, 'index.html')
# Create your views here.

#Add schedules
def schedules(request): 
    if request.method == 'POST':
        form = ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('schedules/list')
        else:
            return HttpResponseRedirect('schedules')
    else:
        form = ScheduleForm()
        return render(request, 'schedules.html', {'form' : form}) 
        #Mengeluarkan form yang digenerate oleh forms.py di shcedules.html

#See list of schedules
def schedulelist(request):
    schd = Schedule.objects.all()
    return render(request, 'schedulelist.html', {'table' : schd})

def deleteschedule(request, idnum):
    Schedule.objects.get(id=idnum).delete()
    schd = Schedule.objects.all()
    return render(request, 'schedulelist.html', {'table' : schd})