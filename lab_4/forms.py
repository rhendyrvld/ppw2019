from django import forms
from .models import Schedule

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule

        fields = ['hari', 'tanggal', 'jam', 'nama_kegiatan', 'tempat', 'kategori']

        widgets = {
            'hari' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
            'tanggal' : forms.DateInput(attrs={'class' : 'form-control', 'type' : 'date'}),
            'jam' : forms.TimeInput(attrs={'class' : 'form-control', 'type' : 'time'}),
            'nama_kegiatan' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
            'tempat' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
            'kategori' : forms.TextInput(attrs={'class' : 'form-control', 'type' : 'text'}),
        }