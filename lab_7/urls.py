from django.urls import *

from . import views

app_name = 'lab_7'
urlpatterns = [
    path('', views.portfolio, name='portfolio')
]