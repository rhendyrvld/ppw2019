from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from datetime import date

# Create your tests here.

class Lab7Test(TestCase):
    def test_portfolio_url_exist(self):
        response = Client().get('/portfolio')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_using_portfolio_func(self):
        found = resolve('/portfolio')
        self.assertEqual(found.func, portfolio)

    def test_portfolio_using_portfolio_template(self):
        response = Client().get('/portfolio')
        self.assertTemplateUsed(response, 'portfolio.html')

    def test_title_in_template(self):
        response = Client().get('/portfolio')
        html_response = response.content.decode()
        self.assertIn("A Little Portfolio", html_response)

    def test_theme_change_button_in_template(self):
        response = Client().get('/portfolio')
        html_response = response.content.decode()
        self.assertIn("Default theme", html_response)